brackets-tools
==============

Adobe Brackets developer tools extension.

	- Highlight Word Selection -> View menu (on/off)
	- New Html5 Template 
	- Remove Empty Lines
	- Remove Break Lines
	- Remove Leading New Lines
	- To Upper Case
	- To Lower Case
	- To Title Case
	- Html Encode
	- Html Docode
	- URL Encode
	- Url Decode
	- Remove line numbers
	- Remove duplicate lines

more tools coming soon...

Highlight Selection Sample (click word and ALT+F3)

![Highlight Selection Sample](http://i58.tinypic.com/2hz1i87.gif)

![Highlight Selection Menu](http://i61.tinypic.com/243f6lh.png)

![Html5 Template](http://i62.tinypic.com/fao7du.png)
